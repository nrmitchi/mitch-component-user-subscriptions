
var Promise = require('bluebird');
var Stripe = require('stripe');
var debug = require('debug')('mitch:component:userSubscription:model:UserSubscription');
var moment = require('moment');
var _ = require('lodash');

var SubscriptionStatuses = {
  Trialing: 'trialing',
  Active: 'active',
  PastDue: 'past_due',
  Canceled: 'canceled',
  Unpaid: 'unpaid'
};


module.exports = function (app) {

  var stripe = app.UserSubscriptions.Stripe;

  function createStripeCustomer (user, options, fn) {
    // Create a customer in Stripe for this user
    debug('createStripeCustomer');

    var userSubscriptionData = app.models.UserSubscription.build({
      status: 'trialing'
    });

    stripe.customers.create({
      description: 'Customer for '+user.id+' ['+user.email+']' // ,
      // source: "tok_161S0Y2eZvKYlo2C80JACsdJ" // obtained with Stripe.js
    }).then(function (customer) {
      // Create the subscription in Stripe for this user
      userSubscriptionData.customer_identifier = customer.id;

      return stripe.customers.createSubscription(customer.id, {
        plan: app.config.userSubscriptions.defaultPlan
      });
    }).then(function (subscription) {
      // Create the UserSubscription model in our system
      userSubscriptionData.subscription_id = subscription.id;

      // We don't update the status here, as Stripe should give us
      // a webhook after the transaction is processed
      return user.setUserSubscription(userSubscriptionData);
    }).then(function (userSubscription) {
      return fn();
    }).catch(function (err) {
      debug(err);
      return fn(err);
    });

  };

  // This is literally only here to expose these methods for testing. 
  // I wish there was a cleaner way of doing it; maybe reaching into 
  // the objects they're attached to?
  module.exports.createStripeCustomer = createStripeCustomer;

  function checkActive () {
    var user = this;

    return new Promise(function (resolve, reject) {
      user.getUserSubscription()
        .then(function (userSubscription) {
          // TODO: Check that when stripe renews, the charge includes those 7 days, 
          //       and it doesn't go 1 month from whenever it was paid

          var graceDays = (_.isUndefined ? app.config.userSubscriptions.graceDays : 3) + 1;

          resolve(moment() < moment(userSubscription.valid_until).add(graceDays, 'days'));
        })
        .catch( reject );
    });
  };

  function addSource (source) {
    return this.getUserSubscription()
      .then(function (userSubscription) {
        return stripe.customers.createSource(
          userSubscription.customer_identifier,
          { source: source })
      });
  };

  return function(sequelize, DataTypes) {

    var modelAttributes = {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },

      // The stripe token for the customer
      customer_identifier: {
        type: DataTypes.STRING
      },

      subscription_id: {
        type: DataTypes.STRING
      },

      // This is a copy of the status in Stripe, and needs webhooks
      // to stay up to date
      status: {
        type: DataTypes.ENUM(_.values(SubscriptionStatuses)),
        allowNull: false,
        defaultValue: SubscriptionStatuses.Trialing
      },

      valid_until: {
        type: DataTypes.DATE
      },

      plan: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: app.config.userSubscriptions.defaultPlan
      },

      notes: {
        type: DataTypes.STRING
      }

    };

    // The attributes in this model should be build from the config
    var UserSubscription = sequelize.define('UserSubscription', modelAttributes, {
      paranoid: true ,
      tableName: 'user_subscriptions' ,
      comment: 'User Subscription database comment' ,
      associate: function(models) {
        models.User.hasOne(UserSubscription, { foreignKey: 'user_id' });
        UserSubscription.belongsTo(models.User, { foreignKey: 'user_id' });

        // Since this will have be fetched this should memoize for like... a day? Go memcached!
        models.User.Instance.prototype.checkActive = checkActive;
        models.User.Instance.prototype.addSource = addSource;

        models.User.addHook('afterCreate', 'createStripeCustomer', createStripeCustomer);
      },
      classMethods: {
        // attributeWhitelist: function(){ return [] }
      },
      instanceMethods: {
        // Note: cb will receive (err, Boolean)
        // getFullname: function() {
        //   return [this.first_name, this.last_name].join(' ');
        // }
      }
    });

    UserSubscription.beforeValidate( function(userSubscription, options, fn) {
      debug('beforeValidate - UserSubscription');
      return fn(null, userSubscription);
    });
    UserSubscription.afterValidate( function(userSubscription, options, fn) {
      debug('afterValidate - UserSubscription');
      return fn(null, userSubscription);
    });
    UserSubscription.beforeCreate( function(userSubscription, options, fn) {
      debug('beforeCreate - UserSubscription');
      return fn();
    });
    UserSubscription.afterCreate( function(userSubscription, options, fn) {
      debug('afterCreate - UserSubscription');
      return fn();
    });
    UserSubscription.beforeUpdate( function(userSubscription, options, fn) {
      debug('beforeUpdate - UserSubscription');
      return fn();
    });
    UserSubscription.afterUpdate( function(userSubscription, options, fn) {
      debug('afterUpdate - UserSubscription');
      return fn();
    });
    UserSubscription.beforeDestroy( function(userSubscription, options, fn) {
      debug('beforeDestroy - UserSubscription');
      return fn();
    });
    UserSubscription.afterDestroy( function(userSubscription, options, fn) {
      debug('afterDestroy - UserSubscription');
      return fn();
    });

    UserSubscription.Status = SubscriptionStatuses;

    return UserSubscription;

  };
};
