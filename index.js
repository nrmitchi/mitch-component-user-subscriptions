
// Export the mount path and server. Warp will looked for 'server' when mounting
module.exports = function (app) {

  app.Errors = app.Errors || {};
  app.Errors.UserSubscriptions = require('./errors');

  return {
    mountPath  : '/' ,
    server     : require('./server.js')(app)
  }

};
