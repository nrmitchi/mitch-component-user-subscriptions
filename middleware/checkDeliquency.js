
var Errors = require('../errors');

module.exports = function checkDeliquency (req, res, next) {
  req.user.checkActive()
    .then(function (active) {
      if (!active) {
        return next(new Errors.ExpiredAccountError());
      }
      return next();
    })
    .catch( next );
};
