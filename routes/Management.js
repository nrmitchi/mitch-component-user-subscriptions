
var Errors = require('../errors');

module.exports = function (app) {
  
  var config = app.config;

  var router = app.Router();

  var stripe = app.UserSubscriptions.Stripe;

  module.exports.getUserSubscription = function getUserSubscription (req, res, next) {

    req.user.getUserSubscription()
      .then( function (userSubscriptions) {
        res.json(userSubscriptions);
      })
      .catch( next );

  };
  router.get('/user/subscription', [ app._middleware.requireUser ], module.exports.getUserSubscription);

  module.exports.updateUserSubscription = function updateUserSubscription (req, res, next) {

    // Create payment object based on callback
    var new_plan = req.body.plan;
    var coupon = req.body.coupon;

    var user = req.user;

    // Todo: This could probably be managed more easily by just checking errors returned
    //       from Stripe when trying to set up on a non-existant plan
    if (app.config.userSubscriptions.validPlans.indexOf(new_plan) < 0) {
      return next(new Errors.InvalidPlanError());
    }

    req.user.getUserSubscription()
      .then(function (userSubscription) {
        return [userSubscription, stripe.customers.updateSubscription(
          userSubscription.customer_identifier,
          userSubscription.subscription_id,
          { plan: new_plan }
        )]
      })
      .spread(function (userSubscription, stripeSubscription) {
        return userSubscription.updateAttributes({
            valid_until : new Date(stripeSubscription.current_period_end * 1000),
            plan: new_plan,
            status: stripeSubscription.status
          }, ['valid_until', 'plan', 'status'])
      })
      .then(function (userSubscription) {
        var addCouponToStripe = null;

        if (coupon) {
          addCouponToStripe = req.user.getUserSubscription()
            .then(function (userSubscription) {
              // Todo: I really want to apply the coupon to the subscription,
              //       not the customer
              return stripe.customers.updateSubscription(
                userSubscription.customer_identifier, 
                userSubscription.subscription_id, {
                coupon: coupon
              })
            });
        };

        return [addCouponToStripe, userSubscription];
      })
      .spread(function (_, userSubscription) {
        return res.status(200).json(userSubscription);
      })
      .catch( next );

  };

  router.put('/user/subscription', [ app._middleware.requireUser ], module.exports.updateUserSubscription);

  /*
   * Update the coupon on the current subscription
   */
  module.exports.updateUserSubscriptionCoupon = function updateUserSubscriptionCoupon (req, res, next) {
    var coupon = req.body.coupon;

    req.user.getUserSubscription()
      .then(function (userSubscription) {
        if (!userSubscription.subscription_id) {
          return next(new Errors.NoSubscriptionError());
        }
        return stripe.customers.updateSubscription(
          userSubscription.customer_identifier,
          userSubscription.subscription_id,
          {
            coupon: coupon
          })
          .then(function (stripeCustomer) {
            return res.status(201).json(stripeCustomer.discount.coupon);
          })
      })
      .catch( next );

  };

  router.put('/user/subscription/coupon', [ app._middleware.requireUser ], module.exports.updateUserSubscriptionCoupon);

  /**
   * Add a source to the users Stripe customer object
   */
  module.exports.addUserSubscriptionSource = function addUserSubscriptionSource (req, res, next) {

    // Create payment object based on callback
    var source = req.body.source;

    req.user.addSource(source)
      .then(function (card) {
        res.status(201).json({});
      })
      .catch( next );
  };

  router.post('/user/subscription/source', [ app._middleware.requireUser ], module.exports.addUserSubscriptionSource);

  module.exports.Router = router;

  return router;

};
