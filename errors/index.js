
var makeError = require('make-error');

// Todo: Build these errors the same way as in authentication
module.exports = {
  ExpiredAccountError: makeError('ExpiredAccountError'),
  InvalidPlanError: makeError('InvalidPlanError'),
  NoSubscriptionError: makeError('NoSubscriptionError'),
};
