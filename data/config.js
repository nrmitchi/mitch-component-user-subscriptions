
/**
 * Payment Configuration
 *
 * Currently this payment module only integrates with Stripe.
 *
 * https://www.petekeen.net/stripe-webhook-event-cheatsheet
 *
 */

module.exports = {

  stripeKey: process.env.STRIPE_API_KEY ,
  // Which stripe events to monitor
  monitoredEvents: ['charge.succeeded'] ,

  validPlans: ['free', 'starter', 'pro'] ,

  defaultPlan: 'free',

  // The number of days to still allow access after the paid period
  // expires. This is useful to avoid prematurely cutting access in the
  // event of a legitimate payment mistake
  graceDays: 3

};
