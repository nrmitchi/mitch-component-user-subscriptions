
module.exports = function (app) {

  var path = require('path') ,
      fs   = require('fs') ,
      _    = require('lodash') ,
      requireDir = require('require-directory') ,
      debug = require('debug')('mitch:component:user-subscriptions:loadMiddleware') ;
  // Require all warp middlewares
  return function (cb) {

    cb = cb || function () {};
    
    debug('Loading middleware:');
    var middlewareDir = path.join(__dirname, '..', 'middleware');
    var result = requireDir(module, middlewareDir, {
      // Todo: Figure out best way to do this
      // visitor: function(obj) {
      //   console.log(obj);
      //   if (_.isFunction(obj)) {
      //     debug('    -> app-ifying require');
      //     return obj(app);
      //   }
      // }
    });

    _.each(result, function (obj, key) {
      debug('  -> %s', key);
    });

    // Tack this middleware onto registry. Conflicts will be overwritten
    // Todo: Consider namespacing middleware to prevent this 
    //       Namespace under module name
    app._middleware = _.extend(app._middleware || {}, result);

    return cb();

  };
};
