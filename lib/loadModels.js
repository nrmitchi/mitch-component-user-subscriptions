
var path = require('path') ;
var _ = require('lodash') ;

module.exports = function (app, config) {

  var moduleName = 'userSubscription';

  return function (cb) {

    cb = cb || function () {};
    /**
      * Todo: Considering using require-directory with the processing arguments to 
      *       load all models in the directory
      */ 

    var models = ['UserSubscription'];

    _.each( models, function (model) {
      var modelPath = path.join(__dirname, '..', 'models', model+'.js');
      app._models.push({
        module  : moduleName,
        Factory : require(modelPath)(app)
      });
    });

    return cb();
  };
};
