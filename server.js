
var mitch            = require('mitch') ,
    assert           = require('assert') ,
    async            = require('async') ,
    util             = require('util') ,
    path             = require('path') ,
    
    router           = mitch.Router() ,
    
    loadMiddleware   = require('./lib/loadMiddleware') ,
    loadModels       = require('./lib/loadModels') ;
    
module.exports = function (app) {
  
  assert(app.config.userSubscriptions, 'UserSubscriptions configuration must exist and be loaded');

  // Create somewhere on app for the module to save stuff (that typically only applies to it)
  app.UserSubscriptions = app.UserSubscriptions || {};
  app.UserSubscriptions.Stripe = require('stripe')(app.config.userSubscriptions.stripeKey);

  loadMiddleware(app)();
  loadModels(app, app.config.userSubscriptions)();

  var managementRouter      = require('./routes/Management')(app);

  router.use(managementRouter);

  return router;

};
