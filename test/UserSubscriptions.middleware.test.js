
var userSubscriptions = require('..');
var path = require('path');
var should = require('should');
var async = require('async');
var _ = require('lodash');
var sinon = require('sinon');
require('sinon-as-promised');
var Promise = require('bluebird');
var checkDeliquency = require('../middleware/checkDeliquency');

describe('user-subscriptions middleware', function(){
  var app;

  before(function (done){
    // Set the node directory to the fixture dir so that everything loads properly
    process.chdir(path.join(__dirname, 'fixtures'));

    app = require('./fixtures/app')();

    app.syncModels()(done);

  });

  after(function (done) {
    async.eachSeries(_.keys(app.models), function (model, callback) {
      app.models[model].drop({cascade: true}).then(function () {
        callback.apply(arguments);
        return null;
      });
    }, function () {
      done();
    });
  });

  describe('checkDeliquency', function() {

    var user;

    before( function (done) {

      // This is basically a really bad stub to prevent this from happening in these tests
      var createUserSubscriptionHook = _.find(app.models.User.options.hooks.afterCreate, function (x) { return typeof x == 'object' && x.name == 'createStripeCustomer'; });
      sinon.stub(createUserSubscriptionHook, 'fn').callsArg(2);

      // Create a user
      user = app.models.User.build({
        email: 'test@test.com',
        password: 'password',
      })

      user.save().then( function (pet) {
        done();
      }).catch( done );
    });

    it('is successfully mounted', function () {
      should.exist(app._middleware.checkDeliquency)
    });

    it('proceeds with error when not active', function (done) {
      // Stub the user.checkActive to resolves False
      user.checkActive = sinon.stub().resolves(false);

      // These are the minimal req/res objects that should be needed. If more, we'll have to pull them from express
      var req = { user: user };
      var res = {};

      app._middleware.checkDeliquency(req, res, function (err) {
        should.exist(err);
        err.name.should.equal('ExpiredAccountError');
        done()
      });
    });

    it('proceeds without error when active', function (done) {
      // Stub the user.checkActive to resolves False
      user.checkActive = sinon.stub().resolves(true);

      // These are the minimal req/res objects that should be needed. If more, we'll have to pull them from express
      var req = { user: user };
      var res = {};

      app._middleware.checkDeliquency(req, res, function (err) {
        should.not.exist(err);
        done()
      });
    });

    it('proceeds with error when isActive rejects', function (done) {
      // Stub the user.checkActive to resolves False
      user.checkActive = sinon.stub().rejects(new Error('test'))

      // These are the minimal req/res objects that should be needed. If more, we'll have to pull them from express
      var req = { user: user };
      var res = {};

      app._middleware.checkDeliquency(req, res, function (err) {
        should.exist(err);
        done()
      });
    });
  });
});
