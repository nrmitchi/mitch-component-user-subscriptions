
var _ = require('lodash');
var mitch = require('mitch');

module.exports = function createApp () {

  var app = mitch();

  app.config = {};
  _.extend(app.config, {
    sequelize: {
      host: 'localhost',
      username: 'ubuntu',
      password: '',
      database: 'circle_test',
      dialect: 'postgres',
      pool: {
        max: 5,
        min: 0,
        idle: 10000
      },
      logging: false
    },
    globals: {}
  });

  _.extend(app.config, {
    userSubscriptions: {
      stripeKey: 'sk_test_Qz9r2Q4q1LoU9TvIZncKiqEU' , // process.env.STRIPE_API_KEY ,
      monitoredEvents: ['charge.succeeded'] ,
      validPlans: ['free', 'starter', 'pro'] ,
      defaultPlan: 'free',
      // The number of days to still allow access after the paid period
      // expires. This is useful to avoid prematurely cutting access in the
      // event of a legitimate payment mistake
      graceDays: 3
    }
  });

  app.loadMiddleware();
  app._middleware = app._middleware || {};
  app._middleware.requireUser = function (_, _, next) { return next(); };
  
  app.loadUserModules();

  // The user-subscriptions module is dependent on having a User model to bind to
  app._models.push({
    module  : 'Users' ,
    Factory : require('./models/user')(app)
  });

  app.loadModels();

  // This is only used for properly checking errors in error cases.
  // In a real app this would be handled at a higher level
  app.use(function (err, req, res, next) {
    res.status(500).json({ 
      type: err.name,
      message: err.message,
      stack: err.stack
    });
  });

  return app;

};
