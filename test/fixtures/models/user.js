
var debug = require('debug')('example:users:model');

module.exports = function (app) {
  return function(sequelize, DataTypes) {
    var User = sequelize.define('User', {  
      email : {
        type : DataTypes.STRING ,
        allowNull : false 
      } , 
      password : {
        type : DataTypes.STRING ,
        allowNull : false
      }
    }, {
      paranoid: true
    });

    return User;
  };
};
