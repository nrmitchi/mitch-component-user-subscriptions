
var userSubscriptions = require('..');
var path = require('path');
var should = require('should');
var async = require('async');
var _ = require('lodash');
var sinon = require('sinon');
require('sinon-as-promised');
var Promise = require('bluebird');
var checkDeliquency = require('../middleware/checkDeliquency');

var stripe = require('stripe')('sk_test_Qz9r2Q4q1LoU9TvIZncKiqEU');

describe('user-subscriptions functions', function(){
  var app;

  before(function (done){
    // Set the node directory to the fixture dir so that everything loads properly
    process.chdir(path.join(__dirname, 'fixtures'));

    app = require('./fixtures/app')();

    app.syncModels()(done);

    // Stub out the stripe functions
    sinon.stub(stripe.customers, "createSubscription").returns(new Promise (function (r, _) { process.nextTick( function () { r({ id: 'sub_123' }); })}));
  });

  after(function (done) {
    async.eachSeries(_.keys(app.models), function (model, callback) {
      app.models[model].drop({cascade: true}).then(function () {
        callback.apply(arguments);
        return null;
      });
    }, function () {
      done();
    });
    stripe.customers.createSubscription.restore();
  });

  describe('User', function() {

    var user;

    before( function (done) {

      // This is basically a really bad stub to prevent this from happening in these tests
      var createUserSubscriptionHook = _.find(app.models.User.options.hooks.afterCreate, function (x) { return typeof x == 'object' && x.name == 'createStripeCustomer'; });
      sinon.stub(createUserSubscriptionHook, 'fn').callsArg(2);

      // Create a user
      user = app.models.User.build({
        email: 'test@test.com',
        password: 'password',
      })

      user.save().then( function (pet) {
        done();
      }).catch( done );
    })

    describe('createStripeCustomer', function () {
      it('is added as a User hook', function () {
        var hook = _.find(app.models.User.options.hooks.afterCreate, function (x) { return typeof x == 'object' && x.name == 'createStripeCustomer'; });
        should.exist(hook);
      });
    });

    describe('checkActive', function () {

      it('is added as a User instance method', function () {
        should.exist(user.checkActive);
      });

      it('returns a Promise', function () {
        sinon.stub(user, 'getUserSubscription').resolves({
          valid_until: new Date('2020-1-1')
        })

        var promise = user.checkActive();
        (typeof promise.then).should.equal('function');

        user.getUserSubscription.restore();
      });

      it('rejects the Promise when getUserSubscription throws', function (done) {
        sinon.stub(user, 'getUserSubscription').rejects(new Error('test'));

        var promise = user.checkActive()
          .then(function () {
            done(new Error('checkActive failed to reject the promise'))
          })
          .catch(function (err) {
            done()
          });

        user.getUserSubscription.restore();
      });

      //Bunch of parameterized should be valid, should not be valid, etc tests
      var tests = [
        {today: '2016-1-1' , valid_until: '2016-1-10', grace_days: 0, expected: true},
        {today: '2016-1-9' , valid_until: '2016-1-10', grace_days: 0, expected: true},
        {today: '2016-1-11' , valid_until: '2016-1-10', grace_days: 0, expected: false},
        {today: '2016-1-11' , valid_until: '2016-1-10', grace_days: 1, expected: true},
        {today: '2016-1-15' , valid_until: '2016-1-10', grace_days: 5, expected: true},
        {today: '2016-1-16' , valid_until: '2016-1-10', grace_days: 5, expected: false},
      ];

      // Todo: Fix the cleanup on this, sinon has a sandbox mode of somekind
      tests.forEach(function(test) {
        it('valid until ' + test.valid_until + ' (' + test.grace_days+ ') on ' + test.today + ' is ' + test.expected, function (done) {
          // Set up stubs
          clock = sinon.useFakeTimers(new Date(test.today).getTime());
          sinon.stub(user, 'getUserSubscription').resolves({
            valid_until: new Date(test.valid_until)
          });
          app.config.userSubscriptions.graceDays = test.grace_days;

          user.checkActive()
            .then(function (active) {
              active.should.equal(test.expected);
              cleanup();
              done();
            })
            .catch( function (err) {
              cleanup();
              done(err);
            })

          // Clean up stubs
          function cleanup () {
            delete(app.config.userSubscriptions.graceDays)
            user.getUserSubscription.restore();
            clock.restore();  
          }
          
        });
      });
    });

    describe('addSource', function () {

      before(function () {
        sinon.stub(app.UserSubscriptions.Stripe.customers, "createSource").resolves({});
      });
      after(function () {
        app.UserSubscriptions.Stripe.customers.createSource.restore();
      });

      it('is added as a User instance method', function () {  
        should.exist(user.addSource);
      });

      it('returns a Promise', function () {
        sinon.stub(user, 'getUserSubscription').resolves({
          customer_identifier: 'cus_123'
        })

        var promise = user.addSource('card_123');
        (typeof promise.then).should.equal('function');

        user.getUserSubscription.restore();
      });
    });
  });
});
