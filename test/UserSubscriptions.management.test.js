
var userSubscriptions = require('..');
var path = require('path');
var should = require('should');
var async = require('async');
var _ = require('lodash');
var sinon = require('sinon');
require('sinon-as-promised');
var Promise = require('bluebird');
var checkDeliquency = require('../middleware/checkDeliquency');

var request = require('supertest');

describe('user-subscriptions endpoints', function(){
  var app;
  var Management
  before(function (done){
    // Set the node directory to the fixture dir so that everything loads properly
    process.chdir(path.join(__dirname, 'fixtures'));

    app = require('./fixtures/app')();

    app.syncModels()(done);

    // Stub out the stripe functions
    sinon.stub(app.UserSubscriptions.Stripe.customers, "createSubscription").resolves({ id: 'sub_123' });
    sinon.stub(app.UserSubscriptions.Stripe.customers, "create").resolves({ customer: { id: 'cus_123' }});
    // Stub out the requireUser
    sinon.stub(app._middleware, 'requireUser').callsArg(2);
  });

  after(function (done) {
    app.UserSubscriptions.Stripe.customers.createSubscription.restore();
    app.UserSubscriptions.Stripe.customers.create.restore();
    app._middleware.requireUser.restore();

    async.eachSeries(_.keys(app.models), function (model, callback) {
      app.models[model].drop({cascade: true}).then(function () {
        callback.apply(arguments);
        return null;
      });
    }, function () {
      done();
    });
  });

  // Todo: I have to find a good way to mock a user on the request, and just test black box behaviour
  describe('Management', function () {

    var express = require('mitch/node_modules/express');
    var user;

    before( function (done) {

      // Create a user
      user = app.models.User.build({
        email: 'test@test.com',
        password: 'password',
      })

      user.save().then( function (pet) {
        done();
      }).catch( done );
   
      // This is really hacky, and is only because I can't get the middleware/functions to mock properly
      express.request.user = user;

    });

    after( function () {
      delete(express.request.user);
    });

    describe('GET /user/subscription', function () {

      // On hold until I can figure out how to get the middleware mocked
      it('calls through requireUser middleware');

      it('returns the correct userSubscription', function (done) {
        request(app)
          .get('/user/subscription')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.user_id.should.equal(user.id);
          })
          .end(done);
      });

      it('handles if getUserSubscription throws', function (done) {

        var errorMessage = 'testErrorMessage';
        sinon.stub(user, 'getUserSubscription').rejects(new Error(errorMessage));

        request(app)
          .get('/user/subscription')
          .expect(500)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.type.should.equal('Error');
            res.body.message.should.equal(errorMessage);
          })
          .end(function (err) {
            user.getUserSubscription.restore();
            done(err);
          });
      });
    });

    describe('PUT /user/subscription', function () {
      // On hold until I can figure out how to get the middleware mocked
      it('calls through requireUser middleware');
    });

    describe('PUT /user/subscription/coupon', function () {
      // On hold until I can figure out how to get the middleware mocked
      it('calls through requireUser middleware');

      it('updates the subscription when valid', function (done) {
        sinon.stub(user, 'getUserSubscription').resolves({
          customer_identifier: 'cus_123',
          subscription_id: 'sub_123',
          valid_until: new Date('2020-1-1')
        });

        sinon.stub(app.UserSubscriptions.Stripe.customers, 'updateSubscription').resolves({
          discount: {
            coupon: {
              code: 'TESTCODE',
              percentage: '50',
              flatAmount: '0'
            }
          }
        });

        request(app)
          .put('/user/subscription/coupon')
          .set('Content-Type', 'application/json')
          .send({
            coupon: 'TESTCODE'
          })
          .expect(201)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            app.UserSubscriptions.Stripe.customers.updateSubscription.called.should.be.true();

            res.body.should.be.instanceof(Object);
            res.body.code.should.equal('TESTCODE');
            res.body.percentage.should.equal('50');
            res.body.flatAmount.should.equal('0');
          })
          .end(function (err) {
            user.getUserSubscription.restore();
            app.UserSubscriptions.Stripe.customers.updateSubscription.restore();
            done(err);
          });

      });

      it('returns an error when no coupon is provided')
      it('returns an error when the coupon does not exist')

      it('returns an error when the user has no userSubscription', function (done) {
        var errorMessage = 'testErrorMessage';
        sinon.stub(user, 'getUserSubscription').rejects(new Error(errorMessage));

        sinon.spy(app.UserSubscriptions.Stripe.customers, 'updateSubscription');

        request(app)
          .put('/user/subscription/coupon')
          .expect(500)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            app.UserSubscriptions.Stripe.customers.updateSubscription.called.should.not.be.true();

            res.body.type.should.equal('Error');
            res.body.message.should.equal(errorMessage);
          })
          .end(function (err) {
            user.getUserSubscription.restore();
            app.UserSubscriptions.Stripe.customers.updateSubscription.restore();
            done(err);
          });
      });

      it('returns an error when the user has no valid Stripe subscription', function (done) {
        var errorMessage = 'testErrorMessage';
        sinon.stub(user, 'getUserSubscription').resolves({
          subscription_id: undefined
        });
        sinon.spy(app.UserSubscriptions.Stripe.customers, 'updateSubscription');

        request(app)
          .put('/user/subscription/coupon')
          .expect(500)
          .expect('Content-Type', /json/)
          .expect(function (res) {

            app.UserSubscriptions.Stripe.customers.updateSubscription.called.should.not.be.true();

            res.body.type.should.equal('NoSubscriptionError');
          })
          .end(function (err) {
            user.getUserSubscription.restore();
            app.UserSubscriptions.Stripe.customers.updateSubscription.restore();
            done(err);
          });
      });

      it('returns an error if Stripe lib errors', function (done) {
        var errorMessage = 'testErrorMessage';

        sinon.stub(user, 'getUserSubscription').resolves({
          customer_identifier: 'cus_123',
          subscription_id: 'sub_123',
          valid_until: new Date('2020-1-1')
        });
        sinon.stub(app.UserSubscriptions.Stripe.customers, 'updateSubscription').rejects(new Error(errorMessage));

        request(app)
          .put('/user/subscription/coupon')
          .expect(500)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.type.should.equal('Error');
            res.body.message.should.equal(errorMessage);
          })
          .end(function (err) {
            user.getUserSubscription.restore();
            app.UserSubscriptions.Stripe.customers.updateSubscription.restore();
            done(err);
          });
      })

    });

    describe('POST /user/subscription/source', function () {
      // On hold until I can figure out how to get the middleware mocked
      it('calls through requireUser middleware');
    });

  });
});
